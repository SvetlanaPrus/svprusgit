/**
 * Напишите функцию для форматирования даты.
 *
 * Фукнция принимает 3 аргумента
 * 1. Дата которую необходимо отформатировать
 * 2. Строка которая содержит желаемый формат даты
 * 3. Разделитель для отформтированной даты
 *
 * Обратите внимание!
 * 1. DD день в формате — 01, 02...31
 * 2. MM месяц в формате — 01, 02...12
 * 3. YYYY год в формате — 2020, 2021...
 * 4. Строка которая обозначает формат даты разделена пробелами
 * 5. В качестве разделителя может быть передано только дефис, точка или слеш
 * 6. Генерировать ошибку если в формате даты присутствет что-то другое кроме DD, MM, YYYY
 * 7. 3-й аргумент опциональный, если он передан не был, то в качестве разделителя используется точка
*/

const formatDate = (date, format, delimiter = '.') => {
    let result;

    const dividers = ['/', '.', '-'];
    const set = ['DD', 'MM', 'YYYY'];

    const DD = date.getDate();
    const MM = date.getMonth();
    const YYYY = date.getFullYear();

    const isDividerValid = function(){
        let result = dividers.includes(delimiter);
        if(result !== true){
            throw new Error('Ошибка разделителя.');
        } else {
            return result;
        }
    }

    const isFormatValid = function(){
        let result;
        const array = format.split(' ');
        const arrayWithFilter = array.filter(el => !set.includes(el));
        if(arrayWithFilter.length !== 0) {
            throw new Error('Ошибка в заданном формате даты. Пример: DD MM YYYY');
        } else {
            return result = true;
        }
    }

    if(isFormatValid() && isDividerValid()){
        const arr = format.split(' ');
        if (arr.length === 3){
            result = [DD, MM, YYYY].join(delimiter);
        } else if (arr.length === 2){
            result = arr.includes('DD')? [DD, MM].join(delimiter) : [MM, YYYY].join(delimiter);
        } else {
            result = arr[0] === 'DD'? DD : arr[0] === 'MM'? MM : YYYY;
        }
        return result;
    }
    return result;
}

console.log(formatDate(new Date(2021, 10, 22), 'DD MM YYYY', '/')); // 22/10/2021
console.log(formatDate(new Date(2021, 10, 22), 'DD MM', '.')); // 22.10
console.log(formatDate(new Date(2021, 10, 22), 'YYYY', '.')); // 2021

