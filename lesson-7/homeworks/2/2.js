/**
 * Доработайте функцию что бы она возвращала объект из переданного вложенного массива
 *
 * Фукнция принимает 1 аргумента
 * 1. Массив из массивов который содержит 2 элемента — [ [ element1, element2 ] ]
 *
 * ЗАПРЕЩЕНО ИСПОЛЬЗОВАТЬ ВСТРОЕННЫЙ МЕТОД Object.fromEntries
 *
 * Обратите внимание!
 * 1. Генерировать ошибку если второй элемент вложенного массива не число, не строка или не null
 * 2. Обязательно использовать деструктуризацию при извлечении элементов массива
 * 3. Если в качестве второго аргумента был передан массив вида [ [ element1, element2 ] ], то его так же нужно преобразовать в объект
 * 4. Для перебора массива можно воспользоваться циклом for..of.
*/

const simpleArray = function ([key, value]) {
    let o = {};

    if (!Array.isArray(value)) {
        if (typeof value !== 'string' && typeof value !== 'number' && value !== null) {
            throw new Error('The second element of array is not valid.')
        } else {
            Object.assign(o, ({[key]: value}));
        }
    } else {
        Object.assign(o, ({[key]: simpleArray(value.flat(Infinity))}));
    }
    return o;
}

const fromEntries = (entries) => {
    const obj = {};
    let key = null, value = null;

    for (let i = 0; i < entries.length; i++){
        [key, value] = entries[i];
        Object.assign(obj, simpleArray([key, value]))
    }
    return obj;
}

console.log(fromEntries([['name', 'John'], ['age', 35]])); // { name: 'John', age: 35 }
console.log(fromEntries([['name', 'John'], ['address', [['city', 'New  York']]]])); // { name: 'John', address: { city: 'New  York' } }

