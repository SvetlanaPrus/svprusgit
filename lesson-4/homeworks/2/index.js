/**
 * Задача 2.
 *
 * Напишите скрипт, который проверяет идентичны ли массивы.
 * Если массивы полностью идентичны - в переменную flag присвоить значение true,
 * иначе - false.
 *
 * Пример 1: const arr1 = [1, 2, 3];
 *           const arr2 = [1, '2', 3];
 *           flag -> false
 *
 * Пример 2: const arr1 = [1, 2, 3];
 *           const arr2 = [1, 2, 3];
 *           flag -> true
 *
 * Пример 3: const arr1 = [];
 *           const arr2 = arr1;
 *           flag -> true
 *
 * Пример 4: const arr1 = [];
 *           const arr2 = [];
 *           flag -> true
 *
 * Условия:
 * - Обязательно проверять являются ли сравниваемые структуры массивами;
 *
*/

const arr1 = [1, 2, 3];
const arr2 = [1, 2, 3];
let flag = '';

// РЕШЕНИЕ
if (Array.isArray(arr1) && Array.isArray(arr2)){
    flag = JSON.stringify(arr1) === JSON.stringify(arr2);
    console.log(flag);
} else {
    console.log('Only array must be used.');
}
