/**
 * Задача 1.
 *
 * Создать функцию calculateAdvanced(), которая в качестве аргументов принимает неограниченное количество функций.
 * При запуске calculateAdvanced() последовательно запускает коллбек-функции из аргументов.
 *
 * Каждая коллбек-функция из цепочки в качестве своего аргумента принимает то, что возвращает предыдущая коллбек-функция.
 *
 * Если на каком-то из вызовов функция-коллбек возбудила ошибку — отловите и сохраните её.
 *
 * После отлова ошибки перейдите к выполнению следующего коллбека.
 *
 * Функция calculateAdvanced() должна возвращать объект с двумя свойствами: `value` и `errors`:
 * - свойство `value` содержит результат вычисления всех функций из цепочки;
 * - свойство `errors` содержит массив с объектами, где каждый объект должен обладать следующими свойствами:
 *     - index — индекс коллбека, на котором ошибка была возбуждена;
 *     - name — имя ошибки;
 *     - message — сообщение ошибки.
 *
 *
 * Если во время выполнения функции-коллбека возникла ошибка, результат выполнения она не вернёт.
 *
 * Поэтому, для продолжения цепочки выполнения
 * необходимо брать результат последней успешно выполненной функции-коллбека.
 *
 * Генерировать ошибки если:
 * - Любой из аргументов не является функцией (в этой задаче, этой ошибкой нужно именно сломать скрипт, отлавливать её не нужно).
 */

// Решение
const calculateAdvanced = function(...functions){
    //if(functions.every(el => el !== 'function')) throw new Error('One of elements is not a function.');

    let err = [], arg = 0, functionResult;

    for(let i = 0; i < functions.length; i++){

        try {
            if (typeof functions[i] !== 'function'){
                throw new Error(`Element at index ${i} is not a function.`);
            } else {
                functionResult = functions[i](arg);

                if(typeof functionResult === 'undefined'){
                    throw new Error(`Callback at index ${i} did not return any value.`);
                } else {
                    arg = functionResult;
                }
            }
        }
        catch(error) {
            err.push({
                index: i,
                name: error.name,
                message: error.message
            })
        };
    }

    let result = {value: arg, errors: err};
    //console.log(result);
    return result;
}

const result = calculateAdvanced(
    '() => {}',    //0
    () => {
        return 7;
    },             //1
    2,             //2
    () => {},      //3
    prevResult => {
        return prevResult + 4;
    },                          //4
    () => {
        throw new RangeError('Range is too big.');
    },                                             //5
    prevResult => {
        return prevResult + 1;
    },                                            //6
    () => {
        throw new ReferenceError('ID is not defined.');
    },                                                  //7
    prevResult => {
        return prevResult * 5;
    },                                   //8
);

console.log(result);


// Функция вернёт:            //*** Неправильно вывел здесь количество ошибок и индексы! См. лист выше.***
// { value: 60,
//     errors:
//      [ { index: 0,
//          name: 'Error',
//          message: 'callback at index 0 did not return any value.' },
//        { index: 2,
//          name: 'Error',
//          message: 'callback at index 2 did not return any value.' },
//        { index: 4, name: 'RangeError', message: 'Range is too big.' },
//        { index: 6,
//          name: 'ReferenceError',
//          message: 'ID is not defined.' } ] }

// try {

// }catch (e) {
//     e.name
//     e.message
// }
