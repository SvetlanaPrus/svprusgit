const divMail = document.createElement("div");
divMail.setAttribute('class', 'form-group');

const labelMail = document.createElement("label");
labelMail.setAttribute('for', 'email');
labelMail.innerHTML = "Электропочта";

const mailInput = document.createElement("input");
mailInput.setAttribute('type', 'email');
mailInput.setAttribute('class', 'form-control');
mailInput.setAttribute('id', 'email');
mailInput.setAttribute('placeholder', 'Введите свою электропочту');

// add all elements to div:
divMail.appendChild(labelMail);
divMail.appendChild(mailInput);

export {divMail}






