const divCheckbox = document.createElement("div");
divCheckbox.setAttribute('class', 'form-group form-check');

const inputCheckbox = document.createElement("input");
inputCheckbox.setAttribute('type', 'checkbox');
inputCheckbox.setAttribute('class', 'form-check-input');
inputCheckbox.setAttribute('id', 'exampleCheck1');

const labelCheckbox = document.createElement("label");
labelCheckbox.setAttribute('class', 'form-check-label');
labelCheckbox.setAttribute('for', 'exampleCheck1');
labelCheckbox.innerHTML = "Запомнить меня";

// add all elements to the div:
divCheckbox.appendChild(inputCheckbox);
divCheckbox.appendChild(labelCheckbox);

export {divCheckbox};
