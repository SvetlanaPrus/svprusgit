/**
 * Создать форму динамически при помощи JavaScript.
 *
 * В html находится пример формы которая должна быть сгенерирована.
 *
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 *
 * Обязательно!
 * 1. Для генерации элементов обязательно использовать метод document.createElement
 * 2. Для установки атрибутов элементам обязательно необходимо использовать document.setAttribute
 * 3. Всем созданным элементам необходимо добавить классы как в разметке
 * 4. После того как динамическая разметка будет готова необходимо удалить код в HTML который находится между комментариями
*/

// РЕШЕНИЕ
import {divMail} from "./elements/inputMail.js";
import {divPassword} from "./elements/inputPassword.js";
import {divCheckbox} from "./elements/checkbox.js";
import {myButton} from "./elements/button.js";

function logInForm() {
    // create the form:
    const myForm = document.createElement("form");
    myForm.setAttribute('id', 'form');

    // add all elements to the form:
    myForm.appendChild(divMail);
    myForm.appendChild(divPassword);
    myForm.appendChild(divCheckbox);
    myForm.appendChild(myButton);

    // add the form inside the body:
    document.getElementsByTagName('body')[0].appendChild(myForm);
}

logInForm();
