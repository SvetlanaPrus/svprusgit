/**
 * Доработать форму из 1-го задания.
 *
 * Добавить обработчик сабмита формы.
 *
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 *
 * Обязательно!
 * 1. При сабмите формы страница не должна перезагружаться
 * 2. Генерировать ошибку если пользователь пытается сабмитить форму с пустыми или содержащими только пробел(ы) полями.
 * 3. Если поля формы заполнены и пользователь нажимает кнопку Вход → вывести в консоль объект следующего вида
 * {
 *  email: 'эмейл который ввёл пользователь',
 *  password: 'пароль который ввёл пользователь',
 *  remember: 'true/false'
 * }
*/

// РЕШЕНИЕ
import {divMail} from "./elements/inputMail.js";
import {divPassword} from "./elements/inputPassword.js";
import {divCheckbox} from "./elements/checkbox.js";
import {myButton} from "./elements/button.js";

function logInForm() {
    // *** CREATE FORM ***
    const myForm = document.createElement("form");
    myForm.setAttribute("id", "form");

    // add all elements to the form:
    myForm.appendChild(divMail);
    myForm.appendChild(divPassword);
    myForm.appendChild(divCheckbox);
    myForm.appendChild(myButton);

    // add the form inside the body:
    document.getElementsByTagName('body')[0].appendChild(myForm);
}

logInForm();

// First - we must create, call the form (see above). After - we can use it (see below).

// *** SUBMIT FORM ***
const form = document.querySelector("#form");

// Comment: pay attention to word "SUBMIT" on the next line, dont write "onsubmit"! its refer to "addEventListener".
form.addEventListener("submit", (event) => {
    event.preventDefault();

    const formData = new FormData(event.target);

    // Comment: pay attention (next lines) that you refer to elements attribute "NAME", not "id" or something else. Its important.
    const email = formData.get('email');
    const password = formData.get('password');
    const remember = formData.get('check') !== null;

    // Comment: This variation also can be used -
    // const dataMail = document.querySelector("#email");
    // const email = dataMail.value;

    const cleanMail = email.trim();
    const cleanPassword = password.trim();

    if (cleanMail.length === 0 || cleanPassword.length === 0){
        throw new Error('Поля формы должны быть заполнены.');
    } else {
        console.log({email, password, remember});
    }
})


