const divPassword = document.createElement("div");
divPassword.setAttribute("class", "form-group");

const labelPassword = document.createElement("label");
labelPassword.setAttribute("for", "password");
labelPassword.innerHTML = "Пароль";

const inputPassword = document.createElement("input");
inputPassword.setAttribute("type", "password");
inputPassword.setAttribute("class", "form-control");
inputPassword.setAttribute("id", "password");
inputPassword.setAttribute("placeholder", "Введите пароль");
inputPassword.setAttribute("value", '');
inputPassword.setAttribute("name", 'password');

divPassword.appendChild(labelPassword);
divPassword.appendChild(inputPassword);

export {divPassword, inputPassword}
