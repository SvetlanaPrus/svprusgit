/**
 * Задача 3.
 *
 * Создайте функцию createFibonacciGenerator().
 *
 * Вызов функции createFibonacciGenerator() должен возвращать объект, который содержит два метода:
 * - print — возвращает число из последовательности Фибоначчи;
 * - reset — обнуляет последовательность и ничего не возвращает.
 *
 * Условия:
 * - Задачу нужно решить с помощью замыкания.
 */

// РЕШЕНИЕ  // Задача оказалась не из легких для меня. Надо было подумать.

function createFibonacciGenerator() {
    let n = 2;

    function makeCounter() {
        return function () {
            return n++;
        };
    }
    let counter = makeCounter();

    return {
        print: function () {
            let arr = [0, 1], i = 1, n = counter();
            for (let k = 3; k <= n; k++) {
                arr.push(arr[i - 1] + arr[i]);
                i = i + 1;
            }
            return arr[arr.length - 1];
        },
        reset: function () {
            n = 2;
        }
    }
}

const generator1 = createFibonacciGenerator();

console.log(generator1.print()); // 1
console.log(generator1.print()); // 1
console.log(generator1.print()); // 2
console.log(generator1.print()); // 3
console.log(generator1.reset()); // undefined
console.log(generator1.print()); // 1
console.log(generator1.print()); // 1
console.log(generator1.print()); // 2

const generator2 = createFibonacciGenerator();

console.log(generator2.print()); // 1
console.log(generator2.print()); // 1
console.log(generator2.print()); // 2

exports.createFibonacciGenerator = createFibonacciGenerator;
