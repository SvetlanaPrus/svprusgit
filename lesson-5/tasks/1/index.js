/**
 * Задача 1.
 *
 * Создайте функцию `f`, которая возвращает куб числа, переданного в качестве аргумента.
 *
 * Условия:
 * - Генерировать ошибку, если в качестве аргумента был передан не числовой тип.
 */

// РЕШЕНИЕ
function f2(num){
    if(typeof num !== 'number' || isNaN(num) === true){
        return 'Error: argument is not a number';
    } else {
        return Math.pow(num, 3);
    }
}

console.log(f2(2)); // 8
