/**
 * Задача 2.
 *
 * Создайте функцию `f`, которая возвращает сумму всех переданных числовых аргументов.
 *
 * Условия:
 * - Функция должна принимать любое количество аргументов;
 * - Генерировать ошибку, если в качестве любого входного аргумента было предано не число.
 */

// РЕШЕНИЕ
function f(){
    let createdArray = Array.from(arguments);
    const isTrue = (currentValue) => typeof currentValue === 'number' && isNaN(currentValue) === false;

    if (createdArray.every(isTrue) === true){
        return createdArray.reduce((sum, current) => sum + current, 0);
    } else {
        return 'Error: one of the given items is not a number.';
    }
}

console.log(f(1, 1, 1, 2, 1, 1, 1, 1)); // 9
